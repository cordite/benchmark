# Cordite Benchmark

Benchmark is where we test the performance of Cordite.
This project contains all the scripts to set up and run the performance tests.

## Emerald Benchmark
This project attempts to perform the [Emerald Benchmark](https://gitlab.com/emerald-platform/emerald/wikis/Emerald-Benchmark) on [Cordite](https://gitlab.com/cordite/) 

![architecture](https://gitlab.com/emerald-platform/emerald/uploads/afdf468ae182fb5214cfe5d5a77383db/EmeraldTest.png)

## Test Execution in Azure

The test uses Azure and can be started using `./scripts/emerald-nodes-create.sh`. This script scales up the number of test nodes as Azure Container Instances in a new Azure resource group. The ACI uses the image from this repo docker registry. `BENCHMARK_REG_USER` and `BENCHMARK_REG_TOKEN` are required to pull the image and values are stored in this repo's environment variables.  

The test can be stopped by deleting the Azure resource group that the test created in the [Azure Portal](https://portal.azure.com/.

Nodes are created round-robin across Azure regions/datacentres in East US, Europe and SE Asia.


## Test on your desktop or single server.

To run it, do the following:

### 1) Clone it 
```
 git clone https://gitlab.com/cordite/cordite.git
```

### 2) Install Docker if you have not already got it

https://www.docker.com/community-edition


### 3) Run the Test network

```
cd test-network

./start_env.sh
```

This will start up a cordite network with 3 client nodes plus a shared database, notary and network map service.

It with then start a 'statsd' server, followed by three benchmark test nodes, one for each client node.

The clients will then start sending each other payments and push their stats to the statsd server.

### 4) View the stats on the dashboard

Follow this link:

[graphite dashboard](http://localhost:7999)

### Stop the benchmark test

./stop_env.sh

# How does the test work?

The performance benchmark test sends batches of payments to random nodes in the cordite network (excluding the dao node and notaries). 
The benchmark aims to find a stable latency between batches and constantly adjusts a target value to gradually find the optimum latency. This adjustment is 'smoothed' by the latency adjustment parameter so that we don't overload the nodes with too many payments too soon - think of it like turning up your central heating and waiting for the thermostat to click off at the target temperature.
The 'batching' is designed so that we can measure parallel throughput as well as latency.
For this reason we set up a number of test accounts (set by the 'numberofuseraccounts' parameter) and
load each account with 1 million tokens. We then send a payment from each of the test accounts in each batch. We wait for the latency time to elapse before sending the next batch.
Setting up the accounts in this way with their own tokens should in theory allow parallel processing in corda.

## Stats produced

The following stats are output to the statsd server:

|statsd statistic|what it measures|
|----------------|----------------|
|cordite_latency_gauge| The latency between sending a payment and getting a response from the finality flow|
|cordite_latency_response_time| Same as above but as a timing object in statsd |
|cordite_payment_complete|This is sent each time a payment is finalised so we can see the frequency in the statsd dashboard|
|cordite_throughput_gauge|Throughput in finalised transactions per second|
|cordite_end_to_end_latency|the latency between a payment being sent and being received by another node - this relies on the nodes clocks being in sync|

## Finality and Finality

Finality is defined in European law as the point at which a payment is 'accepted' by a system and the liability to make that payment 'good' in the event of bankruptcy is on the payer.
This fits nicely with corda finality as this is the point when the transaction has been signed by all parties and notaries. 
Finality in corda does not mean that the payment has reached the target nodes vault, only that all parties agree that the payment is valid and it has been sent to the target node.
For this reason we are also measuring the end to end latency - i.e. the point at which the payment appears in the target vault; you could call this 'user finality' as the end user has access to the funds and is able to spend them.

## Parameters

The following is a comprehensive list of parameters and what they do:

|Parameter Long Name|Parameter Alias|Mandatory|What it does|
|:-------------------|:---------------|:---------|:---------------------------------------------|
|nodeurl|h|Y|braid endpoint address of cordite node|
|collectdhost|c|Y|hostname of the statsd collection server|
|notarylegalname|n|Y|The notary that we want to use for issuing the tokens|
|numberofpaymenttransactions|t|N|This is actually the number of payment 'batches' we run before ending the test. This is superseeded if the test duration parameter is greater than zero|
|testduration|d|N|The length of the test in milliseconds. This value overrides the number of payment transactions parameter|
|testaccountname|a|N|Name of the test account. This is appended with the number of the test account |
|numberofuseraccounts|u|N|This determines the number of user accounts created. A payment is sent for each user account on each test cycle so that payments are sent in batches and we can measure parallel throughput.|
|testtoken|x|N|Name of the test token we use in the test|
|minlatency|l|N|The smallest latency we want between batches in milliseconds|
|maxlatency|m|N|The maximum latency we want between batches in milliseconds|
|latencyadjustment|j|N|We have a latency level that we use to establish a stable latency. If the last latency recorded goes above this latency level then the latency level is adjusted by the amount set in this parameter. The latency level is adjusted until it reaches the maxium or minimum levels. This value acts as a way of smoothing the adjustment so we don't over or under compensate for latency variations.|
|startinglatency|s|N|This is the starting latency - i.e. the interval between batches.|
|connectionretryinterval|i|N|this is how often we'll try connecting to the cordite nodes|
|connectionretries|r|N|The number of times we'll try connecting to a node before we give up and go out for some retail therapy|
|daonodename|o|N|name of the dao node - this is to stop random payments being sent to the dao node.|
|loglevel|g|N|This allows us to restrict the amount of logging, and is the usual log level 'error','warn','info','debug' - If the 'info' level is used the throughput is the only value that is output in the console during the main part of the tests|
|remoteNodeUrls||N|These are the braid endpoints of the other nodes, which we have to use to create remote accounts dynamically|
