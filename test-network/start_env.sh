#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# build environment
# usage ./build_env.sh <cordite image tag> <environment>

IMAGE_TAG=${1:-cordite/cordite:v0.5.3}
ENVIRONMENT_SLUG=${2:-dev}

if [ ${ENVIRONMENT_SLUG} = "min" ]
then
    echo "starting a minimum cordite network"
    declare -a notaries=("bootstrap-notary")
    declare -a nodes=("emea")
    declare -a ports=("9081")
elif [ ${ENVIRONMENT_SLUG} = "docker-test" ]
then
    echo "starting a mini cordite network"
    declare -a notaries=("bootstrap-notary")
    declare -a nodes=("apac emea amer")
    declare -a ports=("9081 9082 9083")
else
    echo "starting a full cordite network"
    declare -a notaries=("bootstrap-notary")
    declare -a nodes=("apac emea amer")
    declare -a ports=("9081 9082 9083")
    declare -a benchmarkers=("benchmarker-apac benchmarker-emea benchmarker-amer")
fi

echo -e "\xE2\x9C\x94 $(date) create environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"
set -e

# clean up any old docker-compose
docker-compose -p ${ENVIRONMENT_SLUG} down

# start NMS (and wait for it to be ready)
# docker login network-map
docker-compose -p ${ENVIRONMENT_SLUG} up -d network-map
until docker-compose -p ${ENVIRONMENT_SLUG} logs network-map | grep -q "io.cordite.networkmap.NetworkMapApp - started"
do
    echo -e "waiting for network-map to start"
    sleep 5
done

# start databases
IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d corda-db
until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs corda-db | grep -q "database system is ready to accept connections"
do
echo -e "waiting for corda-db to start up and register..."
sleep 5
done

# start notaries (and wait for them to be ready)
# docker login cordite
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NOTARY}
done
for NOTARY in $notaries
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NOTARY} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NOTARY} to start up and register..."
    sleep 5
  done
done

# Pause Notaries but not before downloading their NodeInfo-* and whitelist.txt
for NOTARY in $notaries
do
    NODE_ID=$(docker-compose -p ${ENVIRONMENT_SLUG} ps -q ${NOTARY})
    NODEINFO=$(docker exec ${NODE_ID} ls | grep nodeInfo-)
    docker cp ${NODE_ID}:/opt/cordite/${NODEINFO} ${NODEINFO}
    docker exec ${NODE_ID} rm network-parameters
    docker pause ${NODE_ID}
done

# Register notaries with nms:
NMS_JWT=$(curl -X POST "http://localhost:9080/admin/api/login" -H "accept: text/plain" -H "Content-Type: application/json" -d "{ \"user\": \"admin\", \"password\": \"admin\"}")
echo "JWT: ${NMS_JWT}"

for NODEINFO in nodeInfo-*
do
    echo "  registering ${NODEINFO}"
    curl -X POST "http://localhost:9080/admin/api/notaries/nonValidating" -H "accept: text/plain" -H "Content-Type: application/octet-stream" -H "Authorization: Bearer ${NMS_JWT}" --data-binary "@${NODEINFO}"
    rm ${NODEINFO}
    echo -e "\xE2\x9C\x94 copied ${NODEINFO} to ${NMS_ID}"
done

# re-start the notaries
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} restart ${NOTARY}
done


# start regional nodes (and wait for them to be ready)
for NODE in $nodes
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NODE}
done
for NODE in $nodes
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NODE} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NODE} to start up and register..."
    sleep 5
  done
done

# test endpoints
for PORT in $ports
do
    while [[ "$(curl -sSfk -m 5 -o /dev/null -w ''%{http_code}'' https://localhost:${PORT}/api/)" != "200" ]]
    do
    echo -e "waiting for ${PORT} to return 200..."
    sleep 5
    done
done

echo -e "\xE2\x9C\x94 $(date) cordite network runner ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"

# Start the benchmarking components

echo -e "\xE2\x9C\x94 $(date) starting statsd server"
docker-compose -p ${ENVIRONMENT_SLUG} up -d  statsd-graphite

echo -e "\xE2\x9C\x94 $(date) starting benchmark nodes"
for BENCHMARK in $benchmarkers
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${BENCHMARK}
done

for BENCHMARK in $benchmarkers
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${BENCHMARK} | grep -q "Running the test with parameters"
  do
    echo -e "waiting for ${BENCHMARK} to start up and register..."
    sleep 5
  done
done

echo -e "\xE2\x9C\x94 $(date) Benchmark test runnning"
echo -e "\xE2\x9C\x94 Go to http://localhost:7999 to see the statistics"


