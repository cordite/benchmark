#/bin/sh
# start Cordite
java -jar corda.jar --log-to-console --no-local-shell &

# wait for Cordite to start up and find others
sleep 120

node benchmarkscript.js \
    -h $BENCHMARK_BRAID_URL \
    -c $BENCHMARK_DASHBOARD_HOST \
    -n "${BENCHMARK_NOTARY_NAME}" \
    -u $BENCHMARK_USER_ACCOUNTS \
    -g $BENCHMARK_DEBUG_LEVEL \
    -p $BENCHMARK_NODE_PREFIX \
    -z \
    > /opt/cordite/logs/benchmarkscript.log

