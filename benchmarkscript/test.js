const assert = require('assert');
const benchmarkhelpers = require('./benchmarkhelpers.js');


const timeoutTime = 14000
const prefix = "emerald";

var allNodesAndNotariesAndMyInfo = {"allNodes":[{"addresses":[{"host":"amer","port":10002}],
"legalIdentities":[{"name":"OU=emerald Foundation, O=armerald AMER, L=New York City, C=US","owningKey":"GfHq2tTVk9z4eXgyLnC8TVmavuF699U1QJPR1v4QiT9dAQsXehU7nVKKaBcW"}]},{"addresses":[{"host":"guardian-notary","port":10002}],"legalIdentities":[{"name":"OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB","owningKey":"GfHq2tTVk9z4eXgyGyFbVNyidHvGsHCWyoM9PSanqe5fqMrBJquMfz2ZogXf"}]},{"addresses":[{"host":"metering-notary","port":10002}],"legalIdentities":[{"name":"OU=Cordite Foundation, O=Cordite Metering Notary, L=London, C=GB","owningKey":"GfHq2tTVk9z4eXgyUEtfNG79rhhuk4wQvQuxSwXHrMcXyGS5L5cv36oDRcbs"}]},{"addresses":[{"host":"committee","port":10002}],"legalIdentities":[{"name":"OU=Cordite Foundation, O=Cordite Committee, L=London, C=GB","owningKey":"GfHq2tTVk9z4eXgySYV6YUR7tUhbu987JqZXf9knmrCtoi36baxVtF571Dsy"}]},{"addresses":[{"host":"bootstrap-notary","port":10002}],"legalIdentities":[{"name":"OU=Cordite Foundation, O=Cordite Bootstrap Notary, L=London, C=GB","owningKey":"GfHq2tTVk9z4eXgyFQwpJiFUtQVKwJxQbwUsjXmSLZ8LAKsVyrFBNrxKkUss"}]},{"addresses":[{"host":"emea","port":10002}],"legalIdentities":[{"name":"OU=emerald Foundation, O=emerald EMEA, L=London, C=GB","owningKey":"GfHq2tTVk9z4eXgyFrg5PfRT2s4S26yCoV1Ka5PNUwMuyBeXSXi5H9DdYv9d"}]},{"addresses":[{"host":"apac","port":10002}],"legalIdentities":[{"name":"OU=emerald Foundation, O=emerald APAC, L=Singapore, C=SG","owningKey":"GfHq2tTVk9z4eXgyQFzY3vNmfBjVRvwq19yqhpb5uFVH63QmM7pqMxmZf4Ks"}]}],
"notaries":[{"name":"OU=Cordite Foundation, O=Cordite Bootstrap Notary, L=London,C=GB","owningKey":"GfHq2tTVk9z4eXgyFQwpJiFUtQVKwJxQbwUsjXmSLZ8LAKsVyrFBNrxKkUss"},
            {"name":"OU=Cordite Foundation, O=Cordite Metering Notary, L=London, C=GB","owningKey":"GfHq2tTVk9z4eXgyUEtfNG79rhhuk4wQvQuxSwXHrMcXyGS5L5cv36oDRcbs"},
            {"name":"OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB","owningKey":"GfHq2tTVk9z4eXgyGyFbVNyidHvGsHCWyoM9PSanqe5fqMrBJquMfz2ZogXf"}],
"myInfo":{"addresses":[{"host":"amer","port":10002}],"legalIdentities":[{"name":"OU=emerald Foundation, O=emerald AMER, L=New York City, C=US","owningKey":"GfHq2tTVk9z4eXgyLnC8TVmavuF699U1QJPR1v4QiT9dAQsXehU7nVKKaBcW"}]}}
var expectedFilteredPeers = {"0":"OU=emerald Foundation, O=emerald APAC, L=Singapore, C=SG"};

describe('benchmark helper tests', function () {
    describe('node prefix', function () {
        it('should find the right prefix', function() {
            var testLegalName = "OU=emerald Foundation, O=emerald EMEA, L=London, C=GB";
            assert(benchmarkhelpers.legalNameContainsPrefix(testLegalName,prefix));
        });

        it('should really seriously only select nodes with the appropriate prefix',function() {

            allPeers = allNodesAndNotariesAndMyInfo.allNodes.map(node => {
                return node.legalIdentities[0].name;
            });

            var selectedPeers = benchmarkhelpers.getPeersWithPrefix(allPeers,prefix)
            assert(selectedPeers[0]=expectedFilteredPeers["0"]);
        });

        it('should still work if the prefix is blank',function() {

            var blankPrefix="";
            allPeers = allNodesAndNotariesAndMyInfo.allNodes.map(node => {
                return node.legalIdentities[0].name;
            });

            var selectedPeers =benchmarkhelpers.getPeersWithPrefix(allPeers,blankPrefix)
            
            assert(selectedPeers.length==7);

        });

        it('should still work with empty prefixwork on the whole thing',function(){

            var blankPrefix=""; 
            var testParameters = {};
            var daoNodes = ['OU=emerald Foundation, O=emerald Committee, L=London, C=GB'];
            var selectedPeers = benchmarkhelpers.selectPeerNodes(allNodesAndNotariesAndMyInfo,testParameters,daoNodes,blankPrefix);
            console.log("selected peers",selectedPeers);

        });

        it('should work with emerald prefix and empty dao list',function(){

            var blankPrefix="emerald"; 
            var testParameters = {};
            var daoNodes = [""];
            var selectedPeers = benchmarkhelpers.selectPeerNodes(allNodesAndNotariesAndMyInfo,testParameters,daoNodes,blankPrefix);
            console.log("selected peers",selectedPeers);

            assert(selectedPeers.length==2);

        });
    });

});
