const winston = require('winston');
const loglevel = "info";
const Proxy = require('braid-client').Proxy;

const logger = winston.createLogger({
    level: loglevel,
    format: winston.format.splat(),
    format: winston.format.combine(
        winston.format.splat(),
        winston.format.simple()
    ),
    transports: [new winston.transports.Console(),]
});

var legalNameContainsPrefix = function legalNameContainsPrefix(legalName, prefix) {
    var prefixWithO = "O=" + prefix;
    var searchResult = legalName.search(prefixWithO);
    return searchResult > 0;
}

var getPeersWithPrefix = function getPeersWithPrefix(allNodes, prefix) {
    var testPeersTemp = allNodes.filter(peer => {
        return legalNameContainsPrefix(peer, prefix);
    });
    return testPeersTemp;
}

var selectPeerNodes = function (allNodesAndNotariesAndMyInfo, testParameters, daoNodes, prefix) {
    allPeers = allNodesAndNotariesAndMyInfo.allNodes.map(node => {
        return node.legalIdentities[0].name;
    });

    //Get legal names of notaries and me
    let mylegalName = allNodesAndNotariesAndMyInfo.myInfo.legalIdentities[0].name
    testParameters.myLegalName = mylegalName;
    let notaries = allNodesAndNotariesAndMyInfo.notaries.map(notary => { return notary.name });

    //Now remove the notaries, daos and me from all the peers list, just leaving the nodes
    //that we want to interact with.
    var testPeersTemp = allPeers.filter(peer => {
        return (!notaries.includes(peer)) && peer !== mylegalName && (!daoNodes.includes(peer));
    });

    var prefixFilteredPeers = getPeersWithPrefix(testPeersTemp, prefix);
    return prefixFilteredPeers;
}

var createAccountOnRemoteNode = async function (clientProxy, notaryLegalName, accountName) {
    var accountCreateResult = await createTestAccountSync(clientProxy, notaryLegalName, accountName);
    var checkAccountExistingResult = await checkAccountHasBeenCreatedSync(clientProxy, accountName);

    return checkAccountExistingResult;
}

var getClientConnections = async function (urlList) {
    var clientConnectionPromises = [];
    urlList.forEach(url => {
        clientConnectionPromises.push(connectToClient(url));
    });
    return Promise.all(clientConnectionPromises).then(clientProxies => {
        return clientProxies;
    });
}


var connectToClient = function connect(connectionString) {
    let result = new Promise(function (resolve, reject) {
        let proxy = new Proxy({ url: connectionString }, () => {
            resolve(proxy)
        }, null, (err) => { reject(err) }, { strictSSL: false });
    });
    return result;
}

var createRemoteAccounts = async function (clientProxies,notaryLegalName,accountName) {
    var createAccountPromises = [];
    clientProxies.forEach(proxy =proxy => { 
        createAccountPromises.push(createRemoteAccount(proxy, notaryLegalName, accountName));
    });

    return Promise.all(createAccountPromises).then(creationResult => {
        return creationResult;
    });
}

async function createRemoteAccount(proxy, notaryLegalName, accountName) {
    try {
        var result = await createAccountOnRemoteNode(proxy, notaryLegalName, accountName);
        return result;
    }
    catch (err) {
        console.error(err);
    }
}


async function createTestAccountSync(clientProxy, notaryLegalName, nameOfTestAccountToCreate) {
    let response
    try {
        response = await checkOrCreateTestAccount(clientProxy, notaryLegalName, nameOfTestAccountToCreate);
        return response;
    } catch (err) {
        var error = ("can't create test account:" + nameOfTestAccountToCreate + err);
        logger.error(error);
        throw (error);
    }
}

async function checkOrCreateTestAccount(clientProxy, notaryLegalName, nameOfTestAccountToCreate) {
    try {
        var accounts = await clientProxy.ledger.listAccounts().then(accounts => {
            return accounts;
        });

        var foundAccount = false;
        foundAccount = accounts.find(account => {
            if (account.address.accountId === nameOfTestAccountToCreate) {
                return true;
            }
        });

        if (foundAccount) {
            return foundAccount;
        }
        else {
            var createAccountResult = await clientProxy.ledger.createAccount(nameOfTestAccountToCreate, notaryLegalName);
            return createAccountResult;
        }

    } catch (err) {
        logger.error("Error checking and creating test account", err);
    }
}

async function checkAccountHasBeenCreatedSync(clientProxy, testAccountName) {
    let result = await clientProxy.ledger.listAccounts().then(accounts => {
        logger.debug(accounts);
        let foundAccount = false;

        accounts.find(account => {
            if (account.address.accountId === testAccountName) {
                logger.debug("Found account:" + testAccountName);
                foundAccount = true;
            }
        });
        return foundAccount;
    });
    return result;
}

async function waitForABit(waitTimeInMilleseconds) {
    await new Promise(resolve => setTimeout(resolve, waitTimeInMilleseconds));
}

module.exports.legalNameContainsPrefix = legalNameContainsPrefix;
module.exports.getPeersWithPrefix = getPeersWithPrefix;
module.exports.selectPeerNodes = selectPeerNodes;
module.exports.createAccountOnRemoteNode = createAccountOnRemoteNode;
module.exports.connectToClient = connectToClient
module.exports.getClientConnections = getClientConnections
module.exports.createRemoteAccounts = createRemoteAccounts