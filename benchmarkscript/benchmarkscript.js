/*
 *   Copyright 2020, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const Proxy = require('braid-client').Proxy;
const StatsD = require('node-statsd');
const commandLineArgs = require('command-line-args');
const winston = require('winston');
const benchmarkhelpers = require('./benchmarkhelpers.js');


//////////////////////////////////////////
/// Read in the command line, lovely!! ///
////////////////////////// ///////////////
var cli = commandLineArgs([
    { name: 'nodeurl', alias: 'h', type: String, description: 'braid endpoint address of cordite node' },
    { name: 'collectdhost', alias: 'c', type: String, description: "host where the collectd server lives" },
    { name: 'notarylegalname', alias: 'n', type: String, description: "The notary that we want to use for issuing the tokens" },
    { name: 'numberofpaymenttransactions', alias: 't', type: Number, defaultValue: 2, description: 'number of payment transaction batches - this is superseeded by test duration if it is greater than zero' },
    { name: 'testduration', alias: 'd', type: Number, defaultValue: 0, description: 'duration of test in milliseconds, this value overrides the number of payment transactions parameter.' },
    { name: 'testaccountname', alias: 'a', type: String, defaultValue: "emerald-benchmark-account", description: "The test account used to issue and send tokens" },
    { name: 'testtoken', alias: 'x', type: String, defaultValue: "XTS", description: "The test account used to issue and send tokens" },
    { name: 'minlatency', alias: 'l', type: Number, defaultValue: 10000, description: "The target minimum latency in milliseconds" },
    { name: 'maxlatency', alias: 'm', type: Number, defaultValue: 50000, description: "The maximum latency" },
    { name: 'latencyadjustment', alias: 'j', type: Number, defaultValue: 10, description: "The latency adjustment made to rate of sending message after" },
    { name: 'startinglatency', alias: 's', type: Number, defaultValue: 20000, description: "the starting latency in milliseconds" },
    { name: 'connectionretryinterval', alias: 'i', type: Number, defaultValue: 1000, description: "The interval in milliseconds between retrying the connection to the node" },
    { name: 'connectionretries', alias: 'r', type: Number, defaultValue: 60, description: "The number of times we try to connect to the node before giving up" },
    { name: 'daonodename', alias: 'o', type: String, defaultValue: "OU=Cordite Foundation, O=Cordite Committee, L=London, C=GB", description: "basically to stop us sending test payments to the DAO" },
    { name: 'numberofuseraccounts', alias: 'u', type: Number, defaultValue: 10, description: "number of user accounts to simulate" },
    { name: 'loglevel', alias: 'g', type: String, defaultValue: "debug", description: "log level to enable filter out key message" },
    { name: 'nodeprefix', alias: 'p', type: String, defaultValue: "", description: "prefix to the 'O' name for finding other nodes to send payments" },
    { name: 'infinity', alias: 'z', type: Boolean, defaultValue: false, description: "Keep test going to infinity, overides -d and -t, the z is for buzz lightyear BTW" },
    { name: 'batchsizeadjustment', alias: 'b', type: Number, defaultValue: 1, description: "the increment or decrement of batch size when we adjust the rate to move towards the benchmark" },
    { name: 'remoteNodeUrls', type: String, multiple: true, defaultOption: true },
]);

var options = cli.parse();

let loglevel = options.loglevel

const logger = winston.createLogger({
    level: loglevel,
    format: winston.format.splat(),
    format: winston.format.combine(
        winston.format.splat(),
        winston.format.simple()
    ),
    transports: [new winston.transports.Console(), ]
});

logger.info("Started benchmark test with options:\n", JSON.stringify(options));

let nodeUrl = options.nodeurl;
let collectdHost = options.collectdhost;
let notaryLegalName = options.notarylegalname;
let numberOfPaymentTransactions = options.numberofpaymenttransactions;
let testDuration = options.testduration;
let testAccountName = options.testaccountname;
let testToken = options.testtoken;
let minLatency = options.minlatency;
let maxLatency = options.maxlatency;
let startinglatency = options.startinglatency;
let latencyadjustment = options.latencyadjustment;
let connectionRetryInterval = options.connectionretryinterval;
let connectionRetries = options.connectionretries;
let daoNodes = [options.daonodename];
let numberOfUserAccounts = options.numberofuseraccounts;
let nodePrefix = options.nodeprefix;
let infinity = options.infinity;
let batchSizeAdjustment = options.batchsizeadjustment;
let remoteNodeUrls = options.remoteNodeUrls;

let initialTokenIssue = 1000000;

let connectionRetryAttempt = 0;
var clientProxy;
var currentPayment = 1;
let testPeers;
let testStartTime = getTimeAsMilliseconds();

statsdClient = new StatsD(collectdHost);

process.on('unhandledRejection', (err) => {
    console.error("unhandled rejection", err);
    process.exit(1)
})

function connectToNode() {
    clientProxy = new Proxy({ url: nodeUrl }, onOpen, onClose, onError, { strictSSL: false })
}

function onOpen() {
    logger.info("Benchmark test starting " + new Date().getTime())

    var testParameters = {};
    testParameters.testAccountName = testAccountName;
    testParameters.notaryLegalName = notaryLegalName;
    testParameters.testTokenSymbol = testToken;

    //TODO - change numberOfPayments to Number of Accounts
    testParameters.numberOfPayments = numberOfPaymentTransactions;
    testParameters.currentBatchSize = numberOfPaymentTransactions;
    testParameters.minLatency = minLatency;
    testParameters.latencyAdjustment = latencyadjustment;
    testParameters.currentLatency = startinglatency;
    testParameters.maxLatency = maxLatency;
    testParameters.numberOfUserAccounts = numberOfUserAccounts;
    testParameters.batchSizeAdjustment = batchSizeAdjustment;
    testParameters.testAccounts = [];
    testParameters.remoteNodeUrls = remoteNodeUrls;

    return prepareTheTest(testParameters)
}

function reportLatency(latencyResult) {

    let overallLatency = latencyResult.overallLatency;
    let latencyResults = latencyResult.latencyResults.latencyResults;
    latencyResults.forEach(latencyResult => {

        let latency = latencyResult.latency;
        statsdClient.gauge('cordite_latency_gauge', latency);
        statsdClient.timing('cordite_latency_response_time', latency);
        statsdClient.set("cordite_payment_complete", 1);
        logger.debug("Single payment latency: " + latency);
    });

    //Calculate Rate
    let transactionRate = 1000 / (overallLatency / latencyResults.length)
    statsdClient.gauge('cordite_rate_gauge', transactionRate);
    logger.info("txn Rate: " + transactionRate);
}

function prepareTheTest(testParameters) {

    logger.info("Preparing the test")
    return getListOfPeers(testParameters).then(testParameters => {
        logger.debug("test parameters with peers:", testParameters);
        return testParameters
    }).then(testParameters => {
        return getTestNotary(notaryLegalName).then(testNotary => {
            testParameters['testNotary'] = testNotary;
            logger.debug("test parameters with test notary:", testParameters);
            return testParameters;
        });
    }).then(testParameters => {
        return benchmarkhelpers.getClientConnections(testParameters.remoteNodeUrls).then(remoteNodeProxies => {
            testParameters['remoteNodeProxies'] = remoteNodeProxies;
            logger.debug("test parameters with remoteNodeProxies:", testParameters);
            return testParameters;
        });
    }).then(testParameters => {
        logger.debug("TestParameter so far", testParameters);
        return checkOrCreateTestAccounts(testParameters).then(testParameters => {
            return testParameters;
        });
    }).then(testParameters => {
        logger.debug("About to issue tokens, testParameters:", testParameters);
        return getOrCreateTestToken(testParameters).then(testParameters => {
            return testParameters;
        });
    }).then(testParameters => {
        return issueTestTokens(testParameters).then(testParameters => {
            return testParameters;
        });
    }).then(testParameters => {
        return runTheTest(testParameters);
    }).catch(err => {
        throw "Error while starting the test:" + err;
    });
}

function runTheTest(testParameters) {
    logger.info("Running the test with parameters:", testParameters);
    listenAndRecordTransactions();

    return resetTimeAndSend(testParameters);
}

var transactionListener = null;

function listenAndRecordTransactions() {

    transactionListener = clientProxy.ledger.listenForTransactions([], result => {
        let epochTime = Date.now();
        let paymentReference = result.description;
        let paymentReferenceTiming = JSON.parse(paymentReference);

        let endToEndElapsedTime = epochTime - paymentReferenceTiming.sentTime;
        statsdClient.timing('cordite_end_to_end_latency', endToEndElapsedTime);
        logger.debug("End to End Latency: " + endToEndElapsedTime);
    });

}

var sleepTime = 1000;

function getSleepTime() {
    logger.debug("Getting Sleep time: " + sleepTime);
    return sleepTime;
}

function setSleepTime(newSleepTime) {
    sleepTime = newSleepTime
}

function weShouldEndTheTestNow() {

    if (infinity == true) {
        return false;
    }

    if (testDuration > 0) {
        let timeNow = getTimeAsMilliseconds();
        let elapsedTime = timeNow - testStartTime;

        logger.debug("Elapsed Test Time: " + elapsedTime)

        if (elapsedTime > testDuration) {
            logger.debug("Test time Elapsed, total elapsed time: " + elapsedTime)
            return true;
        }
        return false;
    }

    if (currentPayment > numberOfPaymentTransactions) {
        logger.debug("All Payment Batches Sent, total:" + currentPayment);
        return true;
    }
    return false;
}

async function getListOfPeersSync(testParameters) {
    let response
    try {
        response = await getListOfPeers(testParameters);
        return testParameters;
    } catch (err) {
        logger.error('cant get list of peers', err)
        return testParameters;
    }
}

function resetTimeAndSend(testParameters) {
    var result = null;

    logger.debug('\n\n*** In Reset Time and send ***')

    var amendedTestParameters = getListOfPeersSync(testParameters);



    logger.debug("list of peers with parameters", amendedTestParameters);

    setTimeout(f => {
        result = sendAndLoop(testParameters);
        return result.then(latencyResult => {
            reportLatency(latencyResult);

            checkRateAndAdjustIII(testParameters, latencyResult.overallLatency).then(calculatedSleep => {

                sleepTime = calculatedSleep

                setSleepTime(sleepTime);
                logger.debug("New sleep Time: " + sleepTime);
                logger.debug("Batch Latency: " + latencyResult.overallLatency + " Adjusted Latency: " + testParameters.currentLatency);

                logger.debug("current Payment: " + currentPayment + " number of Expected: " + numberOfPaymentTransactions);

                if (weShouldEndTheTestNow()) {
                    logger.debug("Finished");
                    transactionListener.cancel();
                    return ("Complete");
                }
                return resetTimeAndSend(testParameters);
            });

        }).catch(err => {
            var errorText = "Error while calling in timer" + err;
            logger.error(errorText);
            throw errorText;
        });

    }, getSleepTime());
}


function sendAndLoop(testParameters) {
    let result = spawn(function*() {
        try {
            currentPayment++;
            logger.debug("Current Payment Batch: " + currentPayment);
            let timeNow = getTimeAsMilliseconds();
            let latencyResults = yield payRandomPeers(testParameters, currentPayment);
            let overallLatency = getTimeAsMilliseconds() - timeNow;
            return { 'latencyResults': latencyResults, 'overallLatency': overallLatency };
        } catch (err) {
            logger.error("Error trying to do transferToken", err)
            throw err;
        }
    });
    return result;
}

function getTimeAsMilliseconds() {
    const hrtime = process.hrtime();
    let milliSeconds = parseInt(((hrtime[0] * 1e3) + (hrtime[1]) * 1e-6));
    return milliSeconds;
}

function spawn(generatorFunc) {
    function continuer(verb, arg) {
        var result;
        try {
            result = generator[verb](arg);
        } catch (err) {
            return Promise.reject(err);
        }
        if (result.done) {
            return result.value;
        } else {
            return Promise.resolve(result.value).then(onFulfilled, onRejected);
        }
    }
    var onFulfilled = continuer.bind(continuer, "next");
    var onRejected = continuer.bind(continuer, "throw");
    var generator = generatorFunc();
    return onFulfilled();
}

function payRandomPeers(testParameters, currentPayment) {
    var payRandomPeersPromises = [];
    testParameters.testAccounts.forEach(testAccount => {
        payRandomPeersPromises.push(payARandomPeer(testParameters, currentPayment, testAccount.address.accountId));
    });
    return Promise.all(payRandomPeersPromises).then(latencyResults => {
        return { "latencyResults": latencyResults };
    });
}

function payARandomPeer(testParameters, currentPayment, accountName) {
    let peerToSendStuffTo = getARandomPeer(testParameters);
    let fromAccount = accountName;
    let toAccount = accountName + "@" + peerToSendStuffTo;
    let testReference = "RandomPeerTest" + currentPayment.toString().padStart(8, "0");

    let epochTime = Date.now();
    let refPayload = { "testReference": testReference, "sentTime": epochTime };
    let refAsString = JSON.stringify(refPayload);

    try {
        let timeBefore = getTimeAsMilliseconds();
        return clientProxy.ledger.transferToken("10.00", testParameters.testTokenType.descriptor.uri, fromAccount, toAccount, refAsString, testParameters.notaryLegalName).then(result => {
            //return clientProxy.ledger.transferAccountToAccount("10.00", testParameters.testTokenType.descriptor.uri, fromAccount, toAccount, refAsString, testParameters.notaryLegalName).then(result => {
            let elapsedTime = getTimeAsMilliseconds() - timeBefore;
            var latencyResult = { 'client': testParameters.myLegalName, 'reference': refAsString, 'latency': elapsedTime, 'result': result }
            return latencyResult;
        }).catch(err => {
            var errorText = "Failed to Transfer token to random peer, error:" + err;
            logger.error(errorText);
            throw errorText;
        });
    } catch (err) {
        var errorText = "problem setting up the payment" + err;
        logger.error(errorText);
        throw errorText;
    };
}

function getOrCreateTestToken(testParameters) {
    logger.debug("Issue some test tokens, testParameters:", testParameters);

    //Do the tokens types already Exist?
    return clientProxy.ledger.listTokenTypes().then(tokenTypes => {
        var testTokenType = null;

        testTokenType = tokenTypes.find(tokenType => {
            if (tokenType.descriptor.symbol === testParameters.testTokenSymbol) { return true; };
        });

        if (!testTokenType) {
            return clientProxy.ledger.createTokenType(testParameters.testTokenSymbol, 2, testParameters.notaryLegalName).then(testTokenType => {
                testParameters.testTokenType = testTokenType;
                return testParameters;
            }).catch(err => {
                let errorText = "Error Caught while creating tokens:" + err;
                logger.error(errorText);
                throw errorText;
            });
        } else {
            logger.debug("Token Already Exists:", testTokenType);
            testParameters.testTokenType = testTokenType;
            return testParameters;
        }
    }).catch(err => {
        let errorText = "Error Caught while creating tokens:" + err;
        logger.error(errorText);
        throw errorText;
    });
}

function issueTestTokens(testParameters) {
    var issueTestTokenPromises = [];
    testParameters.testAccounts.forEach(testAccount => {
        logger.debug("creating tokens for test account", testAccount);
        issueTestTokenPromises.push(issueTestTokensForAccount(testParameters, testAccount.address.accountId));
    });

    return Promise.all(issueTestTokenPromises).then(testTokenIssueResult => {
        return testParameters;
    });
}

async function issueTestTokensForAccountSync(testParameters, testAccountName) {
    let response
    try {
        response = await issueTestTokensForAccount(testParameters, testAccountName);
        return testParameters;
    } catch (err) {
        logger.error('cant issue tokens for test account:' + testAccountName, err)
        return testParameters;
    }
}

function issueTestTokensForAccount(testParameters, testAccountName) {
    logger.debug("Issuing test tokens for: ", testAccountName);

    let epochTime = Date.now();
    let refPayload = { "testReference": "tonnes of tokens for the test", "sentTime": epochTime };

    return clientProxy.ledger.issueToken(testAccountName, initialTokenIssue, testParameters.testTokenType.descriptor.symbol, JSON.stringify(refPayload), testParameters.notaryLegalName).then(tokensIssued => {
        testParameters.tokensIssued = tokensIssued;
        return testParameters;
    }).then(testParameters => {
        return checkBalanceForAccount(testParameters, testAccountName);
    }).catch(err => {
        let errorText = "Error Caught while creating tokens:" + err;
        logger.error(errorText);
        throw errorText;
    });
}

function checkBalanceForAccount(testParameters, testAccountName) {
    return clientProxy.ledger.balanceForAccount(testAccountName).then(balance => {
        logger.debug("Got balance" + JSON.stringify(balance) + " for account:" + testAccountName);

        let testCurrencyBalance = balance.find(x => {
            return x.amountType.symbol == testParameters.testTokenSymbol && x.amountType.issuerName == testParameters.myLegalName
        });

        if (testCurrencyBalance.quantity >= initialTokenIssue) {
            logger.debug("We have the right balance: ", testCurrencyBalance.quantity);
            return testParameters;
        } else {
            logger.debug("Not Enough test tokens - issuing more");
            return issueTestTokensForAccount(testParameters, testAccountName).then(testParameters => {
                return testParameters;
            });
        }
        return testParameters;
    }).catch(err => {
        console.log(err);
        throw err;
    });
}

function getARandomPeer(testParameters) {
    let whichPeer = Math.floor(Math.random() * testParameters.peers.length);
    return testParameters.peers[whichPeer];
}

function checkOrCreateTestAccounts(testParameters) {
    let testAccountName = testParameters.testAccountName;
    var accountCreationPromises = [];
    for (var accountNumber = 1; accountNumber <= testParameters.numberOfUserAccounts; accountNumber++) {
        var nameOfTestAccountToCreate = createTestAccountName(testAccountName, accountNumber);
        accountCreationPromises.push(checkOrCreateTestAccount(testParameters, nameOfTestAccountToCreate))
    }
    var promiseMaybe = Promise.all(accountCreationPromises).then(whateverthisis => {
        return testParameters;
    }).catch(err => {
        logger.error("Error creating accounts", err)
        throw err;
    });

    return promiseMaybe;
}

function createTestAccountName(testAccountName, testAccountNumber) {
    return testAccountName + "-" + testAccountNumber.toString();
}

async function createTestAccountSync(testParameters, nameOfTestAccountToCreate) {
    let response
    try {
        response = await checkOrCreateTestAccount(testParameters, nameOfTestAccountToCreate);
        return testParameters;
    } catch (err) {
        logger.error('cant create test account:' + nameOfTestAccountToCreate, err)
        return testParameters;
    }
}

async function checkAccountHasBeenCreatedSync(testAccountName) {
    let result = await clientProxy.ledger.listAccounts().then(accounts => {

        let foundAccount = false;

        accounts.find(account => {
            if (account.address.accountId === testAccountName) {
                logger.debug("Found account:" + testAccountName);
                foundAccount = true;
            }
        });
        return foundAccount;
    });
    return result;
}

async function waitForABit(waitTimeInMilleseconds) {
    await new Promise(resolve => setTimeout(resolve, waitTimeInMilleseconds));
}

async function checkOrCreateTestAccount(testParameters, nameOfTestAccountToCreate) {
    logger.debug("\n\nCreating account:" + nameOfTestAccountToCreate);

    var notaryLegalName = testParameters.notaryLegalName;
    var connections = testParameters.remoteNodeProxies;

    try {
        var remoteAccountCreationResults = await benchmarkhelpers.createRemoteAccounts(connections, notaryLegalName, nameOfTestAccountToCreate);
    } catch (err) {
        logger.error("Swallowing Remote Account Creation failure", err);
    }

    return clientProxy.ledger.listAccounts().then(accounts => {
        logger.debug(accounts);
        var foundAccount = accounts.find(account => {
            if (account.address.accountId === nameOfTestAccountToCreate) {
                return true;
            }
        });

        if (foundAccount) {
            testParameters.testAccounts.push(foundAccount);
            return testParameters;
        } else {
            return clientProxy.ledger.createAccount(nameOfTestAccountToCreate, testParameters.notaryLegalName).then(createdAccount => {
                logger.debug(createdAccount, " has been created")
                testParameters.testAccounts.push(createdAccount);
                return testParameters;
            }).catch(error => {
                var errorText = "error creating test account:" + error;
                logger.error(errorText);
                throw errorText;
            });
        }
    });
}

function getTestNotary(testNotaryLegalName) {
    return clientProxy.network.getNotary(testNotaryLegalName);
}

function onClose() {
    logger.info("connection closed, probably for afternoon tea");
}

function onError(err) {
    logger.info('no connection yet:', err)
    connectionRetryAttempt++;

    if (connectionRetryAttempt > connectionRetries) {
        throw "Could not connect to Corda Node" + nodeUrl + " after " + connectionRetryAttempt + " attempts"
    }
    logger.info("Retrying Connection, attempt: " + connectionRetryAttempt);
    setTimeout(connectToNode, connectionRetryInterval);
}

function getListOfPeers(testParameters) {

    return clientProxy.network.notaryIdentities().then(notaries => {
        return notaries;

    }).then(notaries => {
        return clientProxy.network.allNodes().then(allNodes => {
            var allNodesAndNotaries = { 'allNodes': allNodes, 'notaries': notaries }
            return allNodesAndNotaries;
        });
    }).then(allNodesAndNotaries => {
        return clientProxy.network.myNodeInfo().then(myNodeInfo => {
            allNodesAndNotaries['myInfo'] = myNodeInfo;
            return allNodesAndNotaries;
        });
    }).then(allNodesAndNotariesAndMyInfo => {
        testPeers = benchmarkhelpers.selectPeerNodes(allNodesAndNotariesAndMyInfo, testParameters, daoNodes, nodePrefix);
        testParameters.peers = testPeers;
        return testParameters;

    }).catch(error => {
        logger.error(error);
        throw "failed to get all the peers, Error:" + error;
    });
}

async function checkRateAndAdjustIII(testParameters, latency) {

    //We are aiming for at steady rate to be maintained every 10 seconds - see emerald benchmark
    //https://gitlab.com/emerald-platform/emerald/wikis/emerald%20benchmark
    //If we come in under 10 seconds then we increase the number of transactions in a batch
    //If we are over 10 seconds then we reduce the number of transactions in the batch
    logger.debug("Latency:" + latency + " minLatency:" + minLatency);
    if (latency < testParameters.minLatency) {

        //Increase the rate - by rate factor
        let numberOfUserAccounts = testParameters.numberOfUserAccounts;
        let newNumberOfUserAccounts = numberOfUserAccounts + testParameters.batchSizeAdjustment;

        logger.debug("NumberOfUserAccounts: " + numberOfUserAccounts + " New Number of User Accounts: " + newNumberOfUserAccounts);

        if (newNumberOfUserAccounts > numberOfUserAccounts) {
            let accountsToCreate = newNumberOfUserAccounts - numberOfUserAccounts;

            for (newAccount = 0; newAccount < accountsToCreate; newAccount++) {
                //Create the new account
                let testAccountName = createTestAccountName(testParameters.testAccountName, numberOfUserAccounts + newAccount + 1)
                logger.debug("Creating new test account" + testAccountName);
                let returnedTestParameters = await createTestAccountSync(testParameters, testAccountName);

                let accountHasBeenCreated = await checkAccountHasBeenCreatedSync(testAccountName);

                while (!accountHasBeenCreated) {
                    await waitForABit(1000);
                    accountHasBeenCreated = await checkAccountHasBeenCreatedSync(testAccountName);
                    logger.debug("accountHasBeenCreated" + accountHasBeenCreated.toString())
                }

                let returnedTestParametersII = await issueTestTokensForAccountSync(testParameters, testAccountName);
                await waitForABit(1000);
            }
        }

        if (isNaN(newNumberOfUserAccounts)) {
            logger.debug("User accounts is now a NaN");
        } else {
            testParameters.numberOfUserAccounts = newNumberOfUserAccounts;
            logger.debug("Number of User Accounts Increased to:" + testParameters.numberOfUserAccounts);
        }

    } else {
        //Decrease the rate
        let newNumberOfUserAccounts = testParameters.numberOfUserAccounts - testParameters.batchSizeAdjustment;
        logger.debug("New number of accounts:" + newNumberOfUserAccounts)
        if (newNumberOfUserAccounts < 1) {
            newNumberOfUserAccounts = 1
        }

        testParameters.numberOfUserAccounts = newNumberOfUserAccounts;
        logger.debug("Batch Size Reduced to: " + testParameters.numberOfUserAccounts);
    }
    return 1;
}


function start() {
    connectToNode();
}

start();