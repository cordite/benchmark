const Proxy = require('braid-client').Proxy;

const benchmarkhelpers = require('./benchmarkhelpers.js');
const notaryLegalName = "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB";
const testAccountName = "test-1-emerald-benchmark";

async function connectAndCreateAccounts() {
    var connectionUrls = ["https://localhost:8081/api/", "https://localhost:8082/api/"];
    try {
        var connections = await benchmarkhelpers.getClientConnections(connectionUrls)
        var accountCreationResults = await benchmarkhelpers.createRemoteAccounts(connections,notaryLegalName,testAccountName);
        return accountCreationResults;
    }
    catch (err) {
        var error = "Error connecting or creating accounts or something" + err;
        console.log(err);
        throw err;
    }
}

function doit() {
   var resultPromise = connectAndCreateAccounts().then(result => {
        console.log("Final Result", result);
    }).catch(err => {
        console.log("Final Error", err);
    });
    resultPromise.then(e => {process.exit(0)});
}

doit();