#!/bin/bash
# deploy emerald benchmark test
# usage ./emerald-cordite-deploy.sh <nodes> <BENCHMARK_DASHBOARD_HOST> <CORDITE_COMPATIBILITY_ZONE_URL>

IMAGE=registry.gitlab.com/cordite/benchmark:latest
ENVIRONMENT=$(od -x /dev/urandom | head -1 | awk '{print $7$8$9}')
NODES=${1:-3}
BENCHMARK_DASHBOARD_HOST=${2:-cordite-perf.uksouth.cloudapp.azure.com}
BENCHMARK_USER_ACCOUNTS=10
BENCHMARK_NODE_PREFIX=emerald-${ENVIRONMENT}
BENCHMARK_DEBUG_LEVEL="debug"
RESOURCE_GROUP=${BENCHMARK_NODE_PREFIX}
CORDITE_COMPATIBILITY_ZONE_URL=${3:-https://network-map-test.cordite.foundation}
LOCATIONS=("eastus" "westeurope" "southeastasia") # list of azure regions


echo "$(date) deploying ${NODES} Emerald benchmark nodes with NMS ${CORDITE_COMPATIBILITY_ZONE_URL} using image ${IMAGE} into resource group ${RESOURCE_GROUP}"

set -e

# Check if we have variables set to access docker reg
if [ -z "$BENCHMARK_REG_USER" ] ; then
   echo "BENCHMARK_REG_USER not set. Value is in repo secret variables"
   exit 1
fi

if [ -z "$BENCHMARK_REG_TOKEN" ] ; then
   echo "BENCHMARK_REG_TOKEN not set. Value is in repo secret variables"
   exit 1
fi

# Create resource group
#az group delete -y -n ${RESOURCE_GROUP} -o table
az group create -l westeurope -n ${RESOURCE_GROUP} -o table

# Create stats/collection/dashboard
#az group deployment create -n ${RESOURCE_GROUP}-dashboard --template-file emerald-dashboard-deployment.json --parameters containerName=${RESOURCE_GROUP}-dashboard -g ${RESOURCE_GROUP}
#echo "Started dashboard at http://${RESOURCE_GROUP}-dashboard.westeurope.azurecontainer.io"

# Create Nodes
for ((i = 1; i <= $NODES; i++)); do
    LOCATION=${LOCATIONS[$i % ${#LOCATIONS[@]}]} # round-robin
    NAME=${BENCHMARK_NODE_PREFIX}-$i
    az container create  -o table \
    --resource-group ${RESOURCE_GROUP} \
    --location ${LOCATION} \
    --name ${NAME} \
    --dns-name-label ${NAME} \
    --ports 10002 8080 \
    --image ${IMAGE} \
    --environment-variables \
    CORDITE_P2P_ADDRESS=${NAME}.${LOCATION}.azurecontainer.io:10002 \
    CORDITE_LEGAL_NAME="O=${NAME}, OU=Cordite, L=London, C=GB" \
    CORDITE_COMPATIBILITY_ZONE_URL="${CORDITE_COMPATIBILITY_ZONE_URL}" \
    BENCHMARK_DASHBOARD_HOST="${BENCHMARK_DASHBOARD_HOST}" \
    BENCHMARK_NOTARY_NAME="OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB" \
    BENCHMARK_USER_ACCOUNTS="${BENCHMARK_USER_ACCOUNTS}" \
    BENCHMARK_NODE_PREFIX="${BENCHMARK_NODE_PREFIX}" \
    BENCHMARK_DEBUG_LEVEL="${BENCHMARK_DEBUG_LEVEL}" \
    --registry-login-server registry.gitlab.com \
    --registry-username ${BENCHMARK_REG_USER} \
    --registry-password ${BENCHMARK_REG_TOKEN}
    echo "$(date) created node ${NAME} in region ${LOCATION} - https://${NAME}.${LOCATION}.azurecontainer.io:8080/"
    echo "${date} follow logs with az container logs -f -n ${NAME} -g ${RESOURCE_GROUP}"
done

echo "$(date) finished deploying Emerald benchmark nodes with NMS ${CORDITE_COMPATIBILITY_ZONE_URL} using image ${IMAGE} into resource group ${RESOURCE_GROUP}"